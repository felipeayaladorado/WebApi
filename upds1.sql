﻿--
-- Script was generated by Devart dbForge Studio for SQL Server, Version 5.4.273.0
-- Product home page: http://devart.com/dbforge/sql/studio
-- Script date 22/02/2021 22:45:36
-- Server version: 14.00.2037
-- Client version: 
--


USE master
GO

IF DB_NAME() <> N'master' SET NOEXEC ON

--
-- Create database [upds1]
--
PRINT (N'Create database [upds1]')
GO
IF DB_ID('upds1') IS NULL
CREATE DATABASE upds1
GO

--
-- Alter database
--
PRINT (N'Alter database')
GO
ALTER DATABASE upds1
  SET
    ANSI_NULL_DEFAULT OFF,
    ANSI_NULLS OFF,
    ANSI_PADDING OFF,
    ANSI_WARNINGS OFF,
    ARITHABORT OFF,
    AUTO_CLOSE ON,
    AUTO_CREATE_STATISTICS ON,
    AUTO_SHRINK OFF,
    AUTO_UPDATE_STATISTICS ON,
    AUTO_UPDATE_STATISTICS_ASYNC OFF,
    COMPATIBILITY_LEVEL = 140,
    CONCAT_NULL_YIELDS_NULL OFF,
    CURSOR_CLOSE_ON_COMMIT OFF,
    CURSOR_DEFAULT GLOBAL,
    DATE_CORRELATION_OPTIMIZATION OFF,
    DB_CHAINING OFF,
    HONOR_BROKER_PRIORITY OFF,
    MULTI_USER,
    NESTED_TRIGGERS = ON,
    NUMERIC_ROUNDABORT OFF,
    PAGE_VERIFY CHECKSUM,
    PARAMETERIZATION SIMPLE,
    QUOTED_IDENTIFIER OFF,
    READ_COMMITTED_SNAPSHOT OFF,
    RECOVERY FULL,
    RECURSIVE_TRIGGERS OFF,
    TRANSFORM_NOISE_WORDS = OFF,
    TRUSTWORTHY OFF
    WITH ROLLBACK IMMEDIATE
GO

ALTER DATABASE upds1
  SET DISABLE_BROKER
GO

ALTER DATABASE upds1
  SET ALLOW_SNAPSHOT_ISOLATION OFF
GO

ALTER DATABASE upds1
  SET FILESTREAM (NON_TRANSACTED_ACCESS = OFF)
GO

ALTER DATABASE upds1
  SET QUERY_STORE = OFF
GO

USE upds1
GO

ALTER DATABASE SCOPED CONFIGURATION SET MAXDOP = 0;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET MAXDOP = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET LEGACY_CARDINALITY_ESTIMATION = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET LEGACY_CARDINALITY_ESTIMATION = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET PARAMETER_SNIFFING = ON;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET PARAMETER_SNIFFING = PRIMARY;
GO

ALTER DATABASE SCOPED CONFIGURATION SET QUERY_OPTIMIZER_HOTFIXES = OFF;
GO

ALTER DATABASE SCOPED CONFIGURATION FOR SECONDARY SET QUERY_OPTIMIZER_HOTFIXES = PRIMARY;
GO

USE upds1
GO

IF DB_NAME() <> N'upds1' SET NOEXEC ON
GO

--
-- Create table [producto]
--
PRINT (N'Create table [producto]')
GO
IF OBJECT_ID(N'producto', 'U') IS NULL
CREATE TABLE producto (
  id int IDENTITY,
  nombre varchar(50) NULL,
  precio float NULL,
  cantidad float NULL,
  CONSTRAINT PK_producto PRIMARY KEY CLUSTERED (id)
)
ON [PRIMARY]
GO

--
-- Create table [persona]
--
PRINT (N'Create table [persona]')
GO
IF OBJECT_ID(N'persona', 'U') IS NULL
CREATE TABLE persona (
  id int IDENTITY,
  nombre varchar(50) NULL,
  edad int NULL,
  ciudad_id int NULL,
  CONSTRAINT PK_persona PRIMARY KEY CLUSTERED (id)
)
ON [PRIMARY]
GO

--
-- Create table [item]
--
PRINT (N'Create table [item]')
GO
IF OBJECT_ID(N'item', 'U') IS NULL
CREATE TABLE item (
  id int IDENTITY,
  nombre varchar(50) NULL,
  precio float NULL,
  cantidad float NULL,
  CONSTRAINT PK_item PRIMARY KEY CLUSTERED (id)
)
ON [PRIMARY]
GO

--
-- Create table [ciudad]
--
PRINT (N'Create table [ciudad]')
GO
IF OBJECT_ID(N'ciudad', 'U') IS NULL
CREATE TABLE ciudad (
  id int IDENTITY,
  nombre varchar(50) NULL,
  CONSTRAINT PK_ciudad PRIMARY KEY CLUSTERED (id)
)
ON [PRIMARY]
GO
-- 
-- Dumping data for table ciudad
--
SET IDENTITY_INSERT ciudad ON
GO
INSERT ciudad(id, nombre) VALUES (1, N'La Paz')
INSERT ciudad(id, nombre) VALUES (2, N'Oruro')
INSERT ciudad(id, nombre) VALUES (3, N'Potosí')
INSERT ciudad(id, nombre) VALUES (4, N'Sucre')
GO
SET IDENTITY_INSERT ciudad OFF
GO
-- 
-- Dumping data for table item
--
SET IDENTITY_INSERT item ON
GO
INSERT item(id, nombre, precio, cantidad) VALUES (1, N'Auriculares', 250, 30)
INSERT item(id, nombre, precio, cantidad) VALUES (2, N'Estabilizador', 180, 15)
INSERT item(id, nombre, precio, cantidad) VALUES (3, N'Parlantes', 270, 25)
GO
SET IDENTITY_INSERT item OFF
GO
-- 
-- Dumping data for table persona
--
SET IDENTITY_INSERT persona ON
GO
INSERT persona(id, nombre, edad, ciudad_id) VALUES (1, N'felipe', 41, 1)
INSERT persona(id, nombre, edad, ciudad_id) VALUES (3, N'juan', 15, 2)
INSERT persona(id, nombre, edad, ciudad_id) VALUES (4, N'Sergio', 20, 1)
INSERT persona(id, nombre, edad, ciudad_id) VALUES (5, N'pablo', 50, 3)
INSERT persona(id, nombre, edad, ciudad_id) VALUES (6, N'gabriel', 15, 2)
INSERT persona(id, nombre, edad, ciudad_id) VALUES (7, N'daniel', 12, 2)
GO
SET IDENTITY_INSERT persona OFF
GO
-- 
-- Dumping data for table producto
--
SET IDENTITY_INSERT producto ON
GO
INSERT producto(id, nombre, precio, cantidad) VALUES (1, N'Parlantes', 250, 30)
INSERT producto(id, nombre, precio, cantidad) VALUES (2, N'Micrófonos', 180, 25)
INSERT producto(id, nombre, precio, cantidad) VALUES (3, N'Auriculares', 210, 50)
INSERT producto(id, nombre, precio, cantidad) VALUES (4, N'Teclado', 70, 100)
INSERT producto(id, nombre, precio, cantidad) VALUES (5, N'Teclado', 70, 100)
INSERT producto(id, nombre, precio, cantidad) VALUES (6, N'Ratón', 250, 15)
INSERT producto(id, nombre, precio, cantidad) VALUES (7, N'Pantalla de 19', 650, 20)
INSERT producto(id, nombre, precio, cantidad) VALUES (8, N'Estabilizador', 280, 10)
INSERT producto(id, nombre, precio, cantidad) VALUES (9, N'Televisor 55', 4200, 5)
GO
SET IDENTITY_INSERT producto OFF
GO

USE upds1
GO

IF DB_NAME() <> N'upds1' SET NOEXEC ON
GO

--
-- Create foreign key [FK_persona_ciudad] on table [persona]
--
PRINT (N'Create foreign key [FK_persona_ciudad] on table [persona]')
GO
IF OBJECT_ID('dbo.FK_persona_ciudad', 'F') IS NULL
  AND OBJECT_ID('persona', 'U') IS NOT NULL
  AND OBJECT_ID('dbo.ciudad', 'U') IS NOT NULL
  AND EXISTS (
  SELECT 1 FROM sys.columns
  WHERE name = N'ciudad_id' AND object_id = OBJECT_ID(N'persona'))
  AND EXISTS (
  SELECT 1 FROM sys.columns
  WHERE name = N'id' AND object_id = OBJECT_ID(N'dbo.ciudad'))
ALTER TABLE persona
  ADD CONSTRAINT FK_persona_ciudad FOREIGN KEY (ciudad_id) REFERENCES ciudad (id)
GO
SET NOEXEC OFF
GO