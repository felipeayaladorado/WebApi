﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebApi.Controllers
{
    public class ProductoController : ApiController
    {
        [HttpGet]
        public IEnumerable<producto> Get()
        {
            using (upds1Entities bd = new upds1Entities())
            {
                var objProducto = bd.producto.ToList();
                return objProducto;
            }
        }

        [HttpGet]
        public IEnumerable<producto> Get(int id)
        {
            using (upds1Entities bd = new upds1Entities())
            {
                var objProducto = bd.producto.Where(d => d.id == id).ToList();
                return objProducto;
            }
        }

        [HttpGet]
        public IEnumerable<producto> Get(string dato)
        {
            using (upds1Entities bd = new upds1Entities())
            {
                var objProducto = bd.producto.Where(d => d.nombre.Contains(dato)).ToList();
                return objProducto;
            }
        }

        [HttpPost]
        public IHttpActionResult Add(Models.Request.ProductoRequest modelo)
        {
            using (upds1Entities bd = new upds1Entities())
            {
                try
                {
                    var objProducto = new producto();
                    objProducto.nombre = modelo.Nombre;
                    objProducto.precio = modelo.Precio;
                    objProducto.cantidad = modelo.Cantidad;
                    bd.producto.Add(objProducto);
                    bd.SaveChanges();
                }
                catch (Exception)
                {

                    throw;
                }


            }
            return Ok("Producto registrado");
        }
    }
}
