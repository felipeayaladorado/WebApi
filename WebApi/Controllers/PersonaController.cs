﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApi.Models;

namespace WebApi.Controllers
{
    public class PersonaController : ApiController
    {
        [HttpGet]
        public IEnumerable<persona>Get()
        {
            string strCadena = "data source=grace; initial catalog=upds1; user id=sa; password=administrador";
            claseConexion rsQ = new claseConexion("select * from persona",strCadena);
            rsQ.Ejecutar();

            persona P;
            List<persona> datos = new List<persona>();
            foreach (DataRow d in rsQ.DT.Rows)
            {
                P = new persona();
                P.id = Int32.Parse(d["id"].ToString());
                P.nombre = d["nombre"].ToString();
                P.edad  = Int32.Parse(d["edad"].ToString());
                P.ciudad_id= Int32.Parse(d["ciudad_id"].ToString());
                datos.Add(P);
            }
            return datos;
        }
    }
}
