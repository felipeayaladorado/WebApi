﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApi.Models.Request
{
    public class ProductoRequest
    {
        public string Nombre { get; set; }
        public float Precio { get; set; }
        public float Cantidad { get; set; }
    }
}